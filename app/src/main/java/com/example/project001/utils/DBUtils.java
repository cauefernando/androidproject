package com.example.project001.utils;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.project001.model.Client;
import com.example.project001.model.ServiceHistory;
import com.example.project001.model.ServiceOrder;
import com.example.project001.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DBUtils {

    private static ServiceOrder so;
    private static User user;

    private static ArrayList<ServiceHistory> serviceHistoryList;
    private static ArrayList<ServiceOrder> serviceOrderList;
    private static ArrayList<Client> clientList;
    private static ArrayList<User> userList;

    private static Map<ServiceOrder, ArrayList<ServiceHistory>> osmap;

    static{

        DBUtils.serviceHistoryList = new ArrayList<ServiceHistory>();
        DBUtils.serviceOrderList = new ArrayList<ServiceOrder>();
        DBUtils.clientList = new ArrayList<Client>();
        DBUtils.userList = new ArrayList<User>();

        //DBUtils.osmap = new HashMap<ServiceOrder, ArrayList<ServiceHistory>>();

    }

    public static void loadingDB(){
        DBUtils.serviceHistoryList = PersistentUtils.getServiceHistoryList();
        DBUtils.serviceOrderList = PersistentUtils.getServiceOrderList();
        DBUtils.clientList = PersistentUtils.getClientList();
        DBUtils.userList = PersistentUtils.getUserList();
    }

    public static boolean addClient(Client cli){

        for(Client client : DBUtils.clientList){
            if(client.getCnpj().equals(cli.getCnpj()))
                return false;
        }
        DBUtils.clientList.add(cli);
        PersistentUtils.firebaseInsert("client", cli, Client.class, cli.getCnpj());
        return true;
    }

    public static void addService(ServiceOrder so){
        ServiceHistory sh = new ServiceHistory();
        sh.setClient(so.getClient());
        sh.setDescription(so.getDescription());
        sh.setDate(new Date(System.currentTimeMillis()));
        sh.setIdentifier(so.getIdentifier());
        sh.setId(so.getId());
        sh.setStatus(so.getStatus());

        ArrayList<ServiceHistory> history = new ArrayList<ServiceHistory>();
        history.add(sh);

        DBUtils.serviceOrderList.add(so);
        DBUtils.serviceHistoryList.add(sh);
        PersistentUtils.firebaseInsert("serviceorder", so, ServiceOrder.class, so.getId());
        PersistentUtils.firebaseInsert("servicehistory", sh, ServiceHistory.class, "");

    }

    public static void addHistory(ServiceHistory sh){
        DBUtils.serviceHistoryList.add(sh);
        PersistentUtils.firebaseInsert("servicehistory", sh, ServiceHistory.class, "");
    }

    public static ArrayList<ServiceHistory> getServiceHistoryList(ServiceOrder so){
        ArrayList<ServiceHistory> temp = new ArrayList<ServiceHistory>();
        for(ServiceHistory sh : DBUtils.serviceHistoryList){
            if(so.getId().equals(sh.getId())){
                temp.add(sh);
            }
        }
        return temp;
        //return DBUtils.osmap.get(so);
    }

    public static ArrayList<ServiceOrder> getServiceOrderList(){
        return DBUtils.serviceOrderList;
    }

    public static ArrayList<Client> getClientsList(){
        return DBUtils.clientList;
    }

    public static ArrayList<String> getStatusList(){
        ArrayList<String> statuslist = new ArrayList<String>();
        statuslist.addAll(Arrays.asList("Em espera", "Em progresso", "Em teste", "Em homologacao", "Entregue", "Cancelado"));
        return statuslist;
    }

    public static ServiceOrder getSo() {
        return DBUtils.so;
    }

    public static void setSo(ServiceOrder so) {
        DBUtils.so = so;
    }

    public static User getUser() {
        return DBUtils.user;
    }

    public static void setUser(User user){
        DBUtils.user = user;
    }

    public static boolean addUser(User item){
        for(User user : DBUtils.userList){
            if(user.getCpf().equals(item.getCpf()))
                return false;
        }
        DBUtils.userList.add(item);
        PersistentUtils.addUser(item);
        PersistentUtils.firebaseInsert("user", item, User.class, item.getCpf());
        return true;
    }

    public static User getUser(String email){
        for(User i : DBUtils.userList){
            if(i.getEmail().toLowerCase().equals(email.toLowerCase()))
                return i;
        }
        return null;
    }

    public static ArrayList<User> getUserList(){
        return DBUtils.userList;
    }


}
