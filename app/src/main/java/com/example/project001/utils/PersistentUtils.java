package com.example.project001.utils;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.project001.model.Client;
import com.example.project001.model.ServiceHistory;
import com.example.project001.model.ServiceOrder;
import com.example.project001.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PersistentUtils {

    private static FirebaseFirestore database;
    private static FirebaseAuth auth;

    public static void addUser(User user){
        auth = FirebaseAuth.getInstance();
        auth.createUserWithEmailAndPassword(user.getEmail(), user.getPass())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.i("SignUp","conta criada com sucesso!");
                        }else{
                            Log.i("SignUp","Falha na criação da conta de usuário!");
                        }
                    }
                });
    }

    public static void firebaseInsert(String table, Object obj, Class clazz, String vfield) {

        Map<String, Object> map = new HashMap<String, Object>();

        try{
            for(Field field : clazz.getDeclaredFields()){
                field.setAccessible(true);
                if(!field.getName().equals("identifier") || !field.getName().equals("pass")){
                    map.put(field.getName(), field.get(obj));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        if(map.size()!=0){
            database = FirebaseFirestore.getInstance();
            database.collection(table).add(map).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                    Log.i("INSERT","Objeto inserido em " + table);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.i("INSERT","Falha ao tentar inserir objeto em " + table);
                }
            });
        }
        //DBUtils.loadingDB();
    }

    public static ArrayList<User> getUserList(){
        ArrayList<User> userlist = new ArrayList<User>();
        database = FirebaseFirestore.getInstance();
        database.collection("user").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for(DocumentSnapshot ds : task.getResult().getDocuments()){
                    User user = new User();
                    user.setPass(ds.getString("pass"));
                    user.setLevel(ds.getString("level"));
                    user.setCpf(ds.getString("cpf"));
                    user.setEmail(ds.getString("email"));
                    user.setName(ds.getString("name"));
                    userlist.add(user);
                }
            }
        });
        return userlist;
    }

    public static ArrayList<Client> getClientList(){
        ArrayList<Client> clientList = new ArrayList<Client>();
        database = FirebaseFirestore.getInstance();
        database.collection("client").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for(DocumentSnapshot ds : task.getResult().getDocuments()){
                    Client client = new Client();
                    client.setCnpj(ds.getString("cnpj"));
                    client.setResponsible(ds.getString("responsible"));
                    client.setCompany(ds.getString("company"));
                    client.setEmail(ds.getString("email"));
                    client.setCpf(ds.getString("cpf"));
                    clientList.add(client);
                }
            }
        });
        return clientList;
    }

    public static ArrayList<ServiceOrder> getServiceOrderList(){
        ArrayList<ServiceOrder> serviceOrderList = new ArrayList<ServiceOrder>();
        database = FirebaseFirestore.getInstance();
        database.collection("serviceorder").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for(DocumentSnapshot ds : task.getResult().getDocuments()){
                    ServiceOrder so = new ServiceOrder();
                    so.setId(ds.getString("id"));
                    so.setClient(ds.getString("client"));
                    so.setDescription(ds.getString("description"));
                    so.setType(ds.getString("type"));
                    so.setStatus(ds.getString("status"));
                    so.setIniDate(ds.getDate("iniDate"));
                    so.setEndDate(ds.getDate("endDate"));
                    serviceOrderList.add(so);
                }
            }
        });
        return serviceOrderList;
    }

    public static ArrayList<ServiceHistory> getServiceHistoryList(){
        ArrayList<ServiceHistory> serviceOrderList = new ArrayList<ServiceHistory>();
        database = FirebaseFirestore.getInstance();
        database.collection("servicehistory").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for(DocumentSnapshot ds : task.getResult().getDocuments()){
                    ServiceHistory sh = new ServiceHistory();
                    sh.setDate(ds.getDate("date"));
                    sh.setDescription(ds.getString("description"));
                    sh.setClient(ds.getString("client"));
                    sh.setId(ds.getString("id"));
                    serviceOrderList.add(sh);
                }
            }
        });
        return serviceOrderList;
    }

}
