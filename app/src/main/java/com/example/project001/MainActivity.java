package com.example.project001;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.project001.model.User;
import com.example.project001.screens.Index;
import com.example.project001.utils.DBUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private Button btnLogin;

    private TextInputEditText user;
    private TextInputEditText pass;

    private TextView result;

    private static final String FAIL = "Falha na tentativa de login.";

    private FirebaseAuth auth;

    private void login(Class clasz){
        Intent activity = new Intent(this, clasz);
        startActivity(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = findViewById(R.id.btnLogin);
        user = findViewById(R.id.iuser);
        pass = findViewById(R.id.ipass);
        result = findViewById(R.id.result);

        auth = FirebaseAuth.getInstance();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userstr = user.getText().toString();
                String passstr = pass.getText().toString();

                auth.signInWithEmailAndPassword(userstr, passstr)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    Log.i("Login", "login realizado com o usuario : "+userstr);
                                    User user = DBUtils.getUser(userstr);
                                    DBUtils.setUser(user);
                                    login(Index.class);
                                }else{
                                    Log.i("Login", "Falha no login realizado com o usuario : "+userstr);
                                    result.setText(FAIL);
                                    result.setTextColor(Color.RED);
                                }
                            }
                        });

            }
        });

        user.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean b) {

                result.setText("    ");
                result.setTextColor(Color.LTGRAY);

            }

        });

    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        if(currentUser != null){
            currentUser.reload();
        }
    }
}