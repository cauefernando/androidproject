package com.example.project001.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.project001.R;
import com.example.project001.model.Client;
import com.example.project001.model.ServiceOrder;
import com.example.project001.utils.DBUtils;
import com.example.project001.utils.IndexUtils;
import com.example.project001.utils.PersistentUtils;
import com.google.android.material.textfield.TextInputEditText;

public class CadClient extends AppCompatActivity {

    private static final int EMPRESA = 0;
    private static final int CNPJ = 1;
    private static final int RESP = 2;
    private static final int EMAIL = 3;
    private static final int CPF = 4;


    private Button cadcli;

    private String getString(Object obj){
        return ((TextInputEditText)obj).getText().toString();
    }

    private void login(Class clasz){
        Intent activity = new Intent(this, clasz);
        startActivity(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadcli);

        cadcli = findViewById(R.id.btncaduser);

        final Object objects[] = {
                findViewById(R.id.iempresa)
                , findViewById(R.id.icnpj)
                , findViewById(R.id.iresponsavel)
                , findViewById(R.id.iemail)
                , findViewById(R.id.icpf)};

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Cadastro de Cliente");

        cadcli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(getString(objects[EMPRESA]).isEmpty()
                    || getString(objects[CNPJ]).isEmpty()
                    || getString(objects[RESP]).isEmpty()
                    || getString(objects[EMAIL]).isEmpty()
                    || getString(objects[CPF]).isEmpty()){

                    alert.setMessage("\nPreencher todos os campos!");
                    alert.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // nada a se fazer
                        }
                    });

                }else{

                    Client cli = new Client();
                    cli.setIdentifier(IndexUtils.getIndex());
                    cli.setCompany(getString(objects[EMPRESA]));
                    cli.setCnpj(getString(objects[CNPJ]));
                    cli.setResponsible(getString(objects[RESP]));
                    cli.setEmail(getString(objects[EMAIL]));
                    cli.setCpf(getString(objects[CPF]));

                    if(DBUtils.addClient(cli)){
                        DBUtils.loadingDB();
                        alert.setMessage("\nCadastro Realizado com sucesso!");
                        alert.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                for(Object obj : objects){
                                    ((TextInputEditText)obj).setText("");
                                }
                            }
                        });
                        alert.setNegativeButton("Finalizar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                login(Index.class);
                            }
                        });
                    }else{
                        alert.setMessage("\nFalha ao cadastrar cliente, cnpj ja existe!");
                        alert.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // nada a se fazer
                            }
                        });
                    }


                }

                alert.create();
                alert.show();

            }
        });

    }

}
