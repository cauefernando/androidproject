package com.example.project001.screens;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.project001.R;
import com.example.project001.model.ServiceHistory;
import com.example.project001.model.ServiceHistoryAdapter;
import com.example.project001.model.ServiceOrder;
import com.example.project001.utils.DBUtils;
import com.example.project001.utils.PersistentUtils;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Date;

public class ServiceOrderHistory extends AppCompatActivity {

    private ListView socontent;

    private TextInputEditText icomment;
    private AutoCompleteTextView cmbstatusso;

    private Button btnrefresh;

    private void refreshHistory(ServiceOrder so){
        ArrayList<ServiceHistory> shlist = DBUtils.getServiceHistoryList(so);
        if(!shlist.isEmpty()){
            ServiceHistoryAdapter sha = new ServiceHistoryAdapter(this, shlist);
            socontent.setAdapter(sha);
            socontent.setSelection(shlist.size() - 1);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_os);

        socontent = findViewById(R.id.socontent);
        icomment = findViewById(R.id.icomment);
        cmbstatusso = findViewById(R.id.cmbstatusso);

        btnrefresh = findViewById(R.id.btnrefresh);

        ArrayAdapter<String> statuslist = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, DBUtils.getStatusList());
        cmbstatusso.setAdapter(statuslist);

        refreshHistory(DBUtils.getSo());

        btnrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String status = cmbstatusso.getText().toString();
                String comment = icomment.getText().toString();

                ServiceOrder so = DBUtils.getSo();
                ServiceHistory sh = new ServiceHistory();
                sh.setIdentifier(so.getIdentifier());
                sh.setId(so.getId());
                sh.setDate(new Date(System.currentTimeMillis()));
                sh.setClient(so.getClient());
                sh.setDescription(comment);

                if(!status.isEmpty()){
                    sh.setStatus(status);
                    so.setStatus(status);
                }else{
                    sh.setStatus(so.getStatus());
                }

                DBUtils.addHistory(sh);
                //DBUtils.loadingDB();

                refreshHistory(DBUtils.getSo());

            }
        });

    }

}
