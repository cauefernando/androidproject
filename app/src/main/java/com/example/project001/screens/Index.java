package com.example.project001.screens;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.project001.R;
import com.example.project001.model.User;
import com.example.project001.utils.DBUtils;

public class Index extends AppCompatActivity {

    private Button btnmenu0;
    private Button btnmenu1;
    private Button btnmenu2;
    private Button btnmenu3;

    private TextView menuname;
    private TextView menumail;
    private TextView menulevel;

    private void login(Class clasz){
        Intent activity = new Intent(this, clasz);
        startActivity(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnmenu0 = findViewById(R.id.btnmenu0);
        btnmenu1 = findViewById(R.id.btnmenu1);
        btnmenu2 = findViewById(R.id.btnmenu2);
        btnmenu3 = findViewById(R.id.btnmenu3);

        menuname = findViewById(R.id.menuname);
        menumail = findViewById(R.id.menumail);
        menulevel = findViewById(R.id.menutipo);

        User user = DBUtils.getUser();


        if(user != null){

            if(user.getLevel().toLowerCase().equals("colaborador")){
                btnmenu0.setVisibility(View.INVISIBLE);
            }

            String nome = user.getName();
            String email = user.getEmail();
            String level = user.getLevel();

            menuname.setText(nome);
            menumail.setText(email);
            menulevel.setText(level);

        }

        btnmenu0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    login(CadUser.class);
                }
            }
        );

        btnmenu1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    login(CadClient.class);
                }
            }
        );

        btnmenu2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    login(CadService.class);
                }
            }
        );

        btnmenu3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    login(ServiceConsult.class);
                }
            }
        );

    }


}
