package com.example.project001.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.project001.R;
import com.example.project001.model.Client;
import com.example.project001.model.ServiceOrder;
import com.example.project001.model.ServiceOrderAdapter;
import com.example.project001.utils.DBUtils;
import com.example.project001.utils.PersistentUtils;

import java.util.ArrayList;

public class ServiceConsult extends AppCompatActivity {

    private ListView menulist;
    private AutoCompleteTextView cmbstatus;
    private AutoCompleteTextView cmbcli;

    private void loadHistory(){
        Intent activity = new Intent(this, ServiceOrderHistory.class);
        startActivity(activity);
    }

    private void setListAdapter(ArrayList<ServiceOrder> solist){
        ServiceOrderAdapter soadapter = new ServiceOrderAdapter(this, solist);
        menulist.setAdapter(soadapter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult);

        menulist = findViewById(R.id.menulist);

        ArrayList<ServiceOrder> solist = DBUtils.getServiceOrderList();
        if(!solist.isEmpty()){
            setListAdapter(solist);
        }

        cmbstatus = findViewById(R.id.cmbstatus);
        cmbcli    = findViewById(R.id.cmbcli);

        ArrayAdapter<String> statuslist = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, DBUtils.getStatusList());
        cmbstatus.setAdapter(statuslist);

        ArrayList<Client> clientlist = DBUtils.getClientsList();
        if(!clientlist.isEmpty()){
            ArrayList<String> clientnamel = new ArrayList<String>();
            for(Client c : clientlist)
                clientnamel.add(c.getCompany());
            ArrayAdapter<String> clients = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, clientnamel);
            cmbcli.setAdapter(clients);
        }

        cmbstatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String cli = cmbcli.getText().toString();
                String status = cmbstatus.getText().toString();

                ArrayList<ServiceOrder> solist = new ArrayList<ServiceOrder>();
                for(ServiceOrder so : DBUtils.getServiceOrderList()){
                    if(cli.isEmpty()){
                        if(so.getStatus().equals(status))
                            solist.add(so);
                    }else if(status.isEmpty()){
                        if(so.getClient().equals(cli))
                            solist.add(so);
                    }else{
                        if(so.getStatus().equals(status)
                                && so.getClient().equals(cli))
                            solist.add(so);
                    }

                }
                setListAdapter(solist);

            }
        });

        cmbcli.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String cli = cmbcli.getText().toString();
                String status = cmbstatus.getText().toString();

                ArrayList<ServiceOrder> solist = new ArrayList<ServiceOrder>();
                for(ServiceOrder so : DBUtils.getServiceOrderList()){
                    if(cli.isEmpty()){
                        if(so.getStatus().equals(status))
                            solist.add(so);
                    }else if(status.isEmpty()){
                        if(so.getClient().equals(cli))
                            solist.add(so);
                    }else{
                        if(so.getStatus().equals(status)
                                && so.getClient().equals(cli))
                            solist.add(so);
                    }

                }
                setListAdapter(solist);

            }
        });

        menulist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ServiceOrder so = (ServiceOrder) adapterView.getAdapter().getItem(i);
                DBUtils.setSo(so);
                loadHistory();
                Log.i("Infor do Service Order", so.getClient());
            }
        });


    }
}
