package com.example.project001.screens;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.project001.R;
import com.example.project001.model.User;
import com.example.project001.utils.DBUtils;
import com.example.project001.utils.PersistentUtils;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Arrays;

public class CadUser  extends AppCompatActivity {

    private static final String[] LEVELACCESS = {"Colaborador", "Administrador"};

    private TextInputEditText username;
    private TextInputEditText usermail;
    private TextInputEditText usercpf;
    private TextInputEditText userpass;

    private AutoCompleteTextView level;

    private Button btncaduser;

    private boolean cancaduser(){
        if(username.getText().toString().isEmpty()
            || usermail.getText().toString().isEmpty()
            || usercpf.getText().toString().isEmpty()
            || level.getText().toString().isEmpty()
            || userpass.getText().toString().isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    private void clearfields(){
        usermail.setText("");
        username.setText("");
        usercpf.setText("");
        userpass.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caduser);

        username = findViewById(R.id.iusername);
        usermail = findViewById(R.id.iusermail);
        usercpf = findViewById(R.id.iusercpf);
        userpass = findViewById(R.id.iuserpass);
        level = findViewById(R.id.iuserlevel);

        btncaduser = findViewById(R.id.btncaduser);

        ArrayAdapter<String> filterlevel = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, LEVELACCESS);
        level.setAdapter(filterlevel);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Cadastro de Usuário");

        btncaduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!cancaduser()){
                    // fazer algo
                    alert.setMessage("\nÉ necessário preencher todos os campos!");
                    alert.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //nada a se fazer
                        }
                    });
                    alert.create();
                    alert.show();

                    return;
                }

                String name = username.getText().toString();
                String email = usermail.getText().toString();
                String cpf = usercpf.getText().toString();
                String pass = userpass.getText().toString();
                String userlevel = level.getText().toString();

                User user = new User();
                user.setName(name);
                user.setEmail(email);
                user.setCpf(cpf);
                user.setLevel(userlevel);
                user.setPass(pass);

                if(!DBUtils.addUser(user)){
                    alert.setMessage("\nUsuário já é cadastrado!");
                    alert.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //nada a se fazer
                        }
                    });

                }else{
                    DBUtils.loadingDB();
                    alert.setMessage("\nUsuário cadastrado com sucesso!");
                    alert.setNeutralButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //PersistentUtils.firebaseInsert("user",user,User.class, user.getCpf());
                            clearfields();
                        }
                    });

                }
                alert.create();
                alert.show();

            }
        });


    }
}
