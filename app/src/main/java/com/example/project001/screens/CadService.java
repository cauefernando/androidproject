package com.example.project001.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.project001.R;
import com.example.project001.model.Client;
import com.example.project001.model.ServiceOrder;
import com.example.project001.model.User;
import com.example.project001.utils.DBUtils;
import com.example.project001.utils.IndexUtils;
import com.example.project001.utils.PersistentUtils;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class CadService extends AppCompatActivity {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    private static final int MENU_CLI = 0;
    private static final int MENU_SERV = 1;
    private static final int MENU_RESP = 2;
    private static final int INI_DATE = 3;
    private static final int END_DATE = 4;
    private static final int BTN_CAD = 5;
    private static final int BTN_INI_DATE = 6;
    private static final int BTN_END_DATE = 7;

    private int index = 0;

    private String getMenuStr(Object obj){
        return ((AutoCompleteTextView)obj).getText().toString();
    }

    private void login(Class clasz){
        Intent activity = new Intent(this, clasz);
        startActivity(activity);
    }

    private String getTVStr(Object obj){
        return ((TextView)obj).getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cados);

        final Object[] objects = {
                findViewById(R.id.menuclilist)
                , findViewById(R.id.menuservlist)
                , findViewById(R.id.menuresplist)
                , findViewById(R.id.iinidate)
                , findViewById(R.id.ienddate)
                , findViewById(R.id.btncados)
                , findViewById(R.id.btniniDate)
                , findViewById(R.id.btnendDate)};

        ArrayAdapter servicos = new ArrayAdapter(this, android.R.layout.simple_list_item_1, Arrays.asList("Consultoria", "Desenvolvimento", "Manutenção", "Outros"));
        ((AutoCompleteTextView) objects[1]).setAdapter(servicos);

        ArrayList<Client> clientlist = DBUtils.getClientsList();

        if(!clientlist.isEmpty()){
            ArrayList<String> clients = new ArrayList<String>();
            ArrayList<String> resps = new ArrayList<String>();

            for(Client clientname : clientlist) {
                clients.add(clientname.getCompany());
                resps.add(clientname.getResponsible());
            }

            ArrayAdapter clientAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, clients);
            ArrayAdapter respsAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, resps);

            ((AutoCompleteTextView) objects[0]).setAdapter(clientAdapter);
            ((AutoCompleteTextView) objects[2]).setAdapter(respsAdapter);

        }

        MaterialDatePicker.Builder materialBuilder = MaterialDatePicker.Builder.datePicker();
        materialBuilder.setTitleText("Selecione uma Data");

        final MaterialDatePicker data = materialBuilder.build();

        data.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {

            @Override
            public void onPositiveButtonClick(Object selection) {

                Date selectedDate = new Date(Long.parseLong(selection.toString()));

                //Calendar cal = Calendar.getInstance();
                //cal.setTime(selectedDate);
                //cal.add(Calendar.DATE, 1);

                //selectedDate = cal.getTime();

                if(index == 1)
                    ((TextView)objects[3]).setText(sdf.format(selectedDate));
                if(index == 2)
                    ((TextView)objects[4]).setText(sdf.format(selectedDate));

                index = 0;
            }
        });

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Ordem de Servico");

        ((Button)objects[BTN_INI_DATE]).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index = 1;
                data.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
            }
        });

        ((Button)objects[BTN_END_DATE]).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index = 2;
                data.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
            }
        });

        ((Button)objects[BTN_CAD]).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getMenuStr(objects[MENU_CLI]).isEmpty()
                        || getMenuStr(objects[MENU_SERV]).isEmpty()
                        || getMenuStr(objects[MENU_RESP]).isEmpty()
                        || !getTVStr(objects[INI_DATE]).contains("/")
                        || !getTVStr(objects[END_DATE]).contains("/")){

                    alert.setMessage("\nPor favor preencher todos os campos!");
                    alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //nada a se fazer
                        }
                    });
                    alert.create();
                    alert.show();

                }else{

                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = sdf.parse(getTVStr(objects[INI_DATE]));
                        date2 = sdf.parse(getTVStr(objects[END_DATE]));
                    } catch (ParseException e) {
                        Log.e("Erro no parse das Datas",e.getMessage());
                    }

                    ServiceOrder so = new ServiceOrder();
                    so.setIdentifier(IndexUtils.getIndex());
                    so.setId(UUID.randomUUID().toString());
                    so.setDescription(String.format("Criado Ordem de Servico, forma : %s", getMenuStr(objects[MENU_SERV])));
                    so.setClient(getMenuStr(objects[MENU_CLI]));
                    so.setStatus("Em espera");
                    so.setType(getMenuStr(objects[MENU_SERV]));
                    so.setIniDate(date1);
                    so.setEndDate(date2);

                    DBUtils.addService(so);
                    DBUtils.loadingDB();
                    //PersistentUtils.firebaseInsert("serviceorder",so, ServiceOrder.class, "");

                    alert.setMessage("\nCadastro Realizado com sucesso!");
                    alert.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            ((TextView)objects[INI_DATE]).setText("");
                            ((TextView)objects[END_DATE]).setText("");

                        }
                    });
                    alert.setNegativeButton("Finalizar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            login(Index.class);
                        }
                    });

                    alert.create();
                    alert.show();
                }

            }
        });

    }
}
