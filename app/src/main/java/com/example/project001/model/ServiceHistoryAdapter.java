package com.example.project001.model;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.project001.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ServiceHistoryAdapter extends BaseAdapter {

    private static List<ServiceHistory> listServicesHistories = new ArrayList<ServiceHistory>();
    private static LayoutInflater mInflater;

    public ServiceHistoryAdapter(Context context, List<ServiceHistory> listServicesOrders){
        this.listServicesHistories = listServicesOrders;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listServicesHistories.size();
    }

    @Override
    public Object getItem(int i) {
        return listServicesHistories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ServiceHistory sh = this.listServicesHistories.get(i);
        view = this.mInflater.inflate(R.layout.service_history_item, null);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyyy");

        ((TextView) view.findViewById(R.id.shdescricao)).setText(sh.getDescription());
        ((TextView) view.findViewById(R.id.shdata)).setText(sdf.format(sh.getDate()));
        ((TextView) view.findViewById(R.id.shstatus)).setText(sh.getStatus());
        ((TextView) view.findViewById(R.id.shdescricao)).setMovementMethod(new ScrollingMovementMethod());

        return view;
    }
}
