package com.example.project001.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.project001.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ServiceOrderAdapter extends BaseAdapter {

    private static List<ServiceOrder> listServicesOrders = new ArrayList<ServiceOrder>();
    private static LayoutInflater mInflater;

    public ServiceOrderAdapter(Context context, List<ServiceOrder> listServicesOrders){
        this.listServicesOrders = listServicesOrders;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listServicesOrders.size();
    }

    @Override
    public Object getItem(int i) {
        return listServicesOrders.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ServiceOrder so = this.listServicesOrders.get(i);
        view = this.mInflater.inflate(R.layout.service_order_item, null);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyyy");

        ((TextView) view.findViewById(R.id.title)).setText(so.getClient());
        ((TextView) view.findViewById(R.id.description)).setText(so.getType());
        ((TextView) view.findViewById(R.id.status)).setText(so.getStatus());
        ((TextView) view.findViewById(R.id.data)).setText(sdf.format(so.getIniDate()));

        return view;
    }
}
